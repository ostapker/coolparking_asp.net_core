﻿using CoolParking.BL.Models;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Helpers
{
    static class VehicleValidator
    {
        private static readonly string pattern = @"\b[A-Z]{2}-\d{4}-[A-Z]{2}\b";

        public static void ValidateVehicleType(VehicleType vehicleType)
        {
            if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException("Invalid Vehicle Type!");
            }
        }

        public static void ValidateId(string id)
        {
            if (!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException("Incorrect Vehicle Id format! Correct Id format example: \"DV-2345-KJ\"");
            }
        }

        public static void ValidateBalance(decimal balance)
        {
            if(balance < 0)
            {
                throw new ArgumentException("Balance cannot be a negative value!");
            }
        }
    }
}
