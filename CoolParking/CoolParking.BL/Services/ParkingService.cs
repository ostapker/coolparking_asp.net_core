﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public ITimerService WithdrawTimer { get; }
        public ITimerService LogTimer { get; }
        public ILogService LogService { get; }
        private Parking Parking { get; set; }

        public ParkingService(ITimerService withdrwTimer, ITimerService logTimer, ILogService logger)
        {
            WithdrawTimer = withdrwTimer;
            WithdrawTimer.Elapsed += OnWithdraw;

            LogTimer = logTimer;
            LogTimer.Elapsed += OnLog;

            LogService = logger;

            Parking = Parking.Instance;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
            {
                throw new InvalidOperationException("Parking is full!");
            }
            else if (Parking.Vehicles.FindAll(v => v.Id == vehicle.Id).Count > 0)
            {
                throw new ArgumentException("Vehicle with such Id is already parked!");
            }
            Parking.Vehicles.Add(vehicle);
        }

        public Vehicle FindById(string vehicleId)
        {
            var vehicle = Parking.Vehicles.Find(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("There is no vehicle with such Id!");
            }
            return Parking.Vehicles.Find(v => v.Id == vehicleId);
        }

        public void Dispose()
        {
            WithdrawTimer.Dispose();
            LogTimer.Dispose();

            Parking.Instance.Vehicles.Clear();
            Parking.Instance.Transactions.Clear();
            Parking.Balance = 0;
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Capacity - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = Parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("There is no vehicle with such Id!");
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Your balance is negative! You need to pay debts first!");
            }
            Parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = Parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("There is no vehicle with such Id!");
            }
            if (sum < 0)
            {
                throw new ArgumentException("The sum for deposit negative!");
            }
            vehicle.Balance += sum;
        }

        public ReadOnlyDictionary<VehicleType, decimal> GetTarrifs()
        {
            return Parking.Tarrifs;
        }

        public decimal GetFineRatio()
        {
            return Parking.FineRatio;
        }

        private void OnWithdraw(object source, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Vehicles)
            {
                var transaction = new TransactionInfo(vehicle.Id, this);
                vehicle.Balance -= transaction.Sum;
                Parking.Balance += transaction.Sum;
                Parking.Transactions.Add(transaction);
            }
        }

        private void OnLog(object source, System.Timers.ElapsedEventArgs e)
        {
            var logInfo = new StringBuilder();
            foreach(var transaction in GetLastParkingTransactions())
            {
                logInfo.Append(transaction.ToString() + "\n");
            }
            Parking.Transactions.Clear();
            LogService.Write(logInfo.ToString());
        }
    }
}