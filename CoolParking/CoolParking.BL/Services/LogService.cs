﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath) {
            LogPath = logPath;
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File is not found!");
            }
            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            using var streamWriter = File.AppendText(LogPath);
            streamWriter.WriteLine(logInfo);
        }        
    }
}