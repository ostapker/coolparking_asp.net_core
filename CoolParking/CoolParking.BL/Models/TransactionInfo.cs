﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using CoolParking.BL.Services;
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public decimal Sum { get; }

        public string VehicleId { get; }

        public DateTime Time { get; }

        private readonly ParkingService parkingService;

        public TransactionInfo(string vehicleId, ParkingService _parkingService)
        {
            VehicleId = vehicleId;
            Time = DateTime.Now;
            parkingService = _parkingService;
            Sum = CalculateSum();
        }


        private decimal CalculateSum()
        {
            try
            {
                var vehicle = parkingService.FindById(VehicleId);
                var tarrif = parkingService.GetTarrifs()[vehicle.VehicleType];
                var fineRatio = parkingService.GetFineRatio();
                return vehicle.Balance >= tarrif
                    ? tarrif
                    : (vehicle.Balance >= 0
                        ? vehicle.Balance + (tarrif - vehicle.Balance) * fineRatio
                        : tarrif * fineRatio);
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        public override string ToString()
        {
            return string.Format("[{0:HH:mm:ss}]  Vehicle Id = \"{1}\"  withdrawing {2}", Time, VehicleId, Sum);
        }
    }
}