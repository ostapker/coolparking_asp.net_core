﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> lazy = new Lazy<Parking>(() => new Parking());

        public List<Vehicle> Vehicles { get; internal set; }
        public decimal Balance { get; internal set; }
        public int Capacity { get; }
        public ReadOnlyDictionary<VehicleType, decimal> Tarrifs { get; private set; }
        public decimal FineRatio { get; }

        public List<TransactionInfo> Transactions { get; internal set; }

        public static Parking Instance { get => lazy.Value; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
            Balance = Settings.InitialBalance;
            Capacity = Settings.ParkingCapacity;
            Tarrifs = Settings.Tariffs;
            FineRatio = Settings.FineRatio;
        }
    }
}