﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using CoolParking.BL.Helpers;
using System;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            VehicleValidator.ValidateId(id);
            VehicleValidator.ValidateVehicleType(vehicleType);
            VehicleValidator.ValidateBalance(balance);

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
            : this(GenerateRandomRegistrationPlateNumber(), vehicleType, balance) { }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            var result = new StringBuilder(12);

            for(int i = 0; i < 2; i++)
            {
                result.Append(Convert.ToChar(rnd.Next(65, 91)));
            }

            result.Append('-');

            for(int i = 0; i < 4; i++)
            {
                result.Append(rnd.Next(0, 10));
            }

            result.Append('-');
            
            for (int i = 0; i < 2; i++)
            {
                result.Append(Convert.ToChar(rnd.Next(65, 91)));
            }

            return result.ToString();
        }

        public override string ToString()
        {
            return String.Format("\"{0}\"  {1}  balance = {2}", Id.ToString(), VehicleType.ToString(), Balance.ToString());
        }
    }
}